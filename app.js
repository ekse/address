//app.js
App({
    onLaunch: function() {
        let that = this;
        // 先从storage中获取thirdsession
        // 如果获取不到就调后台 that.login()
        // console.log("App onLaunch");
        let thirdSession = wx.getStorageSync('thirdSession');
        if (thirdSession == "") {
            // 登录
            that.login();
        }

        wx.getSystemInfo({
            success: res => {
                let model = res.model;
                if(model.indexOf('iPhone') != -1){
                    that.globalData.isIOS = true;
                }
            }
        })
    },

    login: function() {
        wx.login({
            success: res => {
                // 发送 res.code 到后台换取 openId, sessionKey, unionId
                var that = this;
                wx.request({
                    url: "https://xsounder.com/address/wxlogin/",
                    method: 'POST',
                    data: {
                        code: res.code
                    },
                    header: {
                        'content-type': 'application/x-www-form-urlencoded'
                    },
                    success: function(res) {
                        console.log("thirdSession:" + res.data.thirdSession);
                        wx.setStorageSync("thirdSession", res.data.thirdSession);
                    }
                });
            }
        })
    },

    getUserInfo: function(cb) {
        var that = this
        if (this.globalData.userInfo) {
            typeof cb == "function" && cb(this.globalData.userInfo)
        } else {
            //调用登录接口
            wx.getUserInfo({
                withCredentials: false,
                success: function(res) {
                    that.globalData.userInfo = res.userInfo
                    typeof cb == "function" && cb(that.globalData.userInfo)
                }
            })
        }
    },
    
    globalData: {
        lastCurse: '',
        memberStorage: 'memberInfo',
        isIOS: false
    }
})