//apply.js
//获取应用实例
// 未申请：成为高级用户
// 已申请：审核中
// 已通过：管理中心
// 未通过：未通过
var app = getApp()

Page({
  data: {
    name: "",
    email: "",
    phone: "",
    remark: ""
  },

  onLoad: function() {},

  // 输入框事件函数
  nameInput: function(event) {
    let ipts = event.detail.value;
    this.setData({
      name: ipts
    })
  },
  emailInput: function(event) {
    let ipts = event.detail.value;
    
    this.setData({
      email: ipts
    })
  },
  phoneInput: function(event) {
    let ipts = event.detail.value;
    this.setData({
      phone: ipts
    })
  },
  remarkInput: function(event) {
    let ipts = event.detail.value;
    this.setData({
      remark: ipts
    })
  },

  submitTap: function() {
    // 保存用户申请
    let thirdSession = wx.getStorageSync('thirdSession');
    let name = this.data.name;
    let email = this.data.email;
    let phone = this.data.phone;
    let remark = this.data.remark;
    // 判断用户输入信息是否规范
    // @todo
    if (name.trim() == "" || email.trim() == "" || phone.trim() == "") {
      wx.showToast({
        title: "请完成表格内容",
        icon: "none",
        duration: 2000
      })
      return;
    }
    let reg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$");
    // 判断邮箱格式
    if (email === '' || !reg.test(email)) {
      wx.showToast({
        title: "邮箱输入有误",
        icon: "none",
        duration: 2000
      })
      return;
    }
    // 判断电话号码是否正确
    if (phone.trim().length != 11) {
      wx.showToast({
        title: "手机号码输入有误",
        icon: "none",
        duration: 2000
      })
      return;
    }
    wx.request({
      url: 'https://xsounder.com/address/apply/',
      method: 'POST',
      data: {
        thirdSession: thirdSession,
        name: name,
        email: email,
        phone: phone,
        remark: remark
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function(res) {
        wx.showToast({
          title: '提交成功，请耐心等待!',
          icon: 'none',
          duration: 2000,
          mask: true,
          success: function() {
            setTimeout(function() {
              wx.navigateBack();
            }, 2000)
          }
        })
      }
    });
  }
})