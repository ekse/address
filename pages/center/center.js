//center.js
/* 
* 1、展示用户的当日信息：新增用户、新增订单、交易金额
* 2、展示用户的总用户数、总订单数
*/
//获取应用实例
var app = getApp()

Page({
    data: {},

    onLoad: function() {},

    onShow: function() {
        let that = this;
        let memberStorageKey = app.globalData.memberStorage;
        let memberInfo = wx.getStorageSync(memberStorageKey);
        // let totalCustomerNum = memberInfo['totalCustomerNum'];
        // let todayTotalPrice = memberInfo['todayTotalPrice'];
        // let totalOrderNum = memberInfo['totalOrderNum'];
        // let todayOrderNum = memberInfo['todayOrderNum'];
        // let todayCustomerNum = memberInfo['todayCustomerNum'];

        that.setData(memberInfo);
    },

    viewClients: function() {
        wx.navigateTo({
            url: "/pages/clientManage/clients/clients"
        })
    },

    viewOrders: function() {
        wx.navigateTo({
            url: "/pages/orderManage/orders/orders"
        })
    },

    viewAccount: function() {
        wx.showToast({
            title: '功能开发中',
            icon: 'none',
            duration: 2000
        })
    }
})