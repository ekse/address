//orders.js
//获取应用实例
var app = getApp()

Page({
    data: {
        "orders": [
            // {
            //     "id": 1,
            //     "clientId": 1,
            //     "name": "胥智",
            //     "phone": "18280086457",
            //     "productName": "火锅底料",
            //     "productNumber": "2",
            //     "date": "2018-10-24",
            //     "province": "四川",
            //     "city": "成都",
            //     "country": "青羊区",
            //     "detail": "成飞大道",
            //     "email": "5thgfka@gmail.com",
            //     "sex": "男",
            //     "productPrice": 80,
            //     "orderRemark": ""
            // },
            // {
            //     "id": 2,
            //     "clientId": 1,
            //     "name": "胥智",
            //     "phone": "18280086457",
            //     "productName": "火锅底料",
            //     "productNumber": "2",
            //     "date": "2018-10-24",
            //     "province": "四川",
            //     "city": "成都",
            //     "country": "青羊区",
            //     "detail": "成飞大道",
            //     "email": "5thgfka@gmail.com",
            //     "sex": "男",
            //     "productPrice": 80,
            //     "orderRemark": ""
            // },
            // {
            //     "id": 3,
            //     "clientId": 1,
            //     "name": "胥智",
            //     "phone": "18280086457",
            //     "productName": "火锅底料",
            //     "productNumber": "2",
            //     "date": "2018-10-24",
            //     "province": "四川",
            //     "city": "成都",
            //     "country": "青羊区",
            //     "detail": "成飞大道",
            //     "email": "5thgfka@gmail.com",
            //     "sex": "男",
            //     "productPrice": 80,
            //     "orderRemark": ""
            // },
        ],
        startX: "",
        delBtnWidth: 180,
    },

    onLoad: function() {},

    onShow: function() {
        let that = this;
        let thirdSession = wx.getStorageSync('thirdSession');
        wx.request({
            url: 'https://xsounder.com/address/getOrders/',
            method: 'POST',
            data: {
                thirdSession: thirdSession,
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                // console.log(res)
                that.setData(res.data)
            }
        });
    },

    onNewTap: function() {
        wx.navigateTo({
            url: "/pages/orderManage/add/add"
        })
    },

    onSearchTap: function() {
        wx.navigateTo({
            url: "/pages/orderManage/search/search"
        })
    },

    touchS: function(e) {
        if (e.touches.length == 1) {
            let sx = e.touches[0].clientX;
            // console.log("touchS", sx);
            this.setData({
                //设置触摸起始点水平方向位置
                startX: sx
            });
        }
    },
    touchM: function(e) {
        if (e.touches.length == 1) {
            //手指移动时水平方向位置
            let moveX = e.touches[0].clientX;
            //手指起始点位置与移动期间的差值
            let disX = this.data.startX - moveX;
            let delBtnWidth = this.data.delBtnWidth;
            let style = "";
            if (disX == 0 || disX < 0) { //如果移动距离小于等于0，说明向右滑动，文本层位置不变
                style = "left:0px";
            } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离
                style = "left:-" + disX + "px";
                if (disX >= delBtnWidth) {
                    //控制手指移动距离最大值为删除按钮的宽度
                    style = "left:-" + delBtnWidth + "px";
                }
            }
            //获取手指触摸的是哪一项
            let index = e.currentTarget.dataset.index;
            let orders = this.data.orders;
            
            orders.map(function(client, idx){
                if(idx == index) {
                    client.style = style;
                } else {
                    client.style = "";
                }
            })

            // console.log("touchM", orders);
            //更新列表的状态
            this.setData({
                orders: orders
            });
        }
    },
    touchE: function(e) {
        if (e.changedTouches.length == 1) {
            //手指移动结束后水平位置
            let endX = e.changedTouches[0].clientX;
            //触摸开始与结束，手指移动的距离
            let disX = this.data.startX - endX;
            let delBtnWidth = this.data.delBtnWidth;
            //如果距离小于删除按钮的1/2，不显示删除按钮
            let style = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
            //获取手指触摸的是哪一项
            let index = e.currentTarget.dataset.index;
            let orders = this.data.orders;
            orders[index].style = style;
            // console.log("touchE", disX)
            //更新列表的状态
            this.setData({
                orders: orders
            });
        }
    },

    onEditTap: function(e){
        let orderId = e.currentTarget.dataset.orderid;
        let clientId = e.currentTarget.dataset.clientid;
        let name = e.currentTarget.dataset.name;
        let province = e.currentTarget.dataset.province;
        let city = e.currentTarget.dataset.city;
        let country = e.currentTarget.dataset.country;
        let detail = e.currentTarget.dataset.detail;
        let phone = e.currentTarget.dataset.phone;
        let sex = e.currentTarget.dataset.sex;
        let email = e.currentTarget.dataset.email;
        let productPrice = e.currentTarget.dataset.productprice;
        let productName = e.currentTarget.dataset.productname;
        let productNumber = e.currentTarget.dataset.productnumber;
        let orderRemark = e.currentTarget.dataset.orderremark;
        //跳转页面
        wx.navigateTo({
            url: "/pages/orderManage/edit/edit?orderId=" + orderId + "&clientId=" + clientId + "&name=" + name + "&province=" + province 
                + "&city=" + city + "&country=" + country + "&detail=" + detail + "&phone=" + phone 
                + "&sex=" + sex + "&email=" + email + "&productPrice=" + productPrice + "&productName=" + productName + "&productNumber=" + productNumber + "&orderRemark=" + orderRemark
        })
    },
    onDeleteTap: function(e){
        let orderId = e.currentTarget.dataset.orderid;
        let that = this;
        wx.showModal({
            title: "提示",
            content: "确定删除订单信息吗？",
            success: function(res){
                if (res.confirm) {
                    let thirdSession = wx.getStorageSync('thirdSession');
                    wx.request({
                        url: 'https://xsounder.com/address/deleteOrder/',
                        method: 'POST',
                        data: {
                            thirdSession: thirdSession,
                            id: orderId
                        },
                        header: {
                            'content-type': 'application/x-www-form-urlencoded'
                        },
                        success: function(res) {
                            // console.log(res)
                            // that.setData(res.data)
                            let tmpOrders = that.data.orders;
                            let orders = [];

                            tmpOrders.map(function(o){
                                if(o.id != orderId) {
                                    orders.push(o);
                                }
                            });

                            that.setData({
                                orders: orders
                            });
                        }
                    });
                } else if (res.cancel) {
                    console.log('用户点击取消', orderId);
                }
            }
        })
    },

    onViewTap: function(e){
        let orderId = e.currentTarget.dataset.orderid;
        let name = e.currentTarget.dataset.name;
        let clientId = e.currentTarget.dataset.clientid;
        let province = e.currentTarget.dataset.province;
        let city = e.currentTarget.dataset.city;
        let orderDate = e.currentTarget.dataset.orderdate;
        let country = e.currentTarget.dataset.country;
        let detail = e.currentTarget.dataset.detail;
        let phone = e.currentTarget.dataset.phone;
        let productPrice = e.currentTarget.dataset.productprice;
        let orderRemark = e.currentTarget.dataset.orderremark;
        let productName = e.currentTarget.dataset.productname;
        let productNumber = e.currentTarget.dataset.productnumber;
        //跳转页面
        let url = "/pages/orderManage/order/order?orderId=" + orderId + "&clientId=" + clientId + "&clientName=" + name + "&province=" + province 
                + "&city=" + city + "&country=" + country + "&detail=" + detail + "&orderPhone=" + phone 
                + "&productPrice=" + productPrice + "&orderDate=" + orderDate + "&productName=" + productName + "&productNumber=" + productNumber + "&orderRemark=" + orderRemark;

        wx.navigateTo({
            url: url
        })
    }
})