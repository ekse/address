//add.js
//获取应用实例
var app = getApp()

Page({
  data: {
    client: {
        "id": 1,
        "name": "",
        "phone": "",
        "province": "",
        "city": "",
        "country": "",
        "detail": "",
        "sex": "",
        "email": "",
        "remark": "",
    },
    productName: "",
    productNumber: "",
    productPrice: "",
    items: [
        {name: '1', value: '男'},
        {name: '0', value: '女'},
    ],
    isClientSelect: false
  },
  
  onLoad: function (options) {
  },
  
  onShow: function(){
    let that = this;
    let clientInfos = wx.getStorageSync("selectedClient");
    if(clientInfos != ""){
        that.setData({
            isClientSelect: true,
            client: clientInfos
        }, function(){
            wx.removeStorageSync("selectedClient");
        })
    }else {
        console.log(0);
    }
  },

  chooseClient: function(){
    wx.navigateTo({
        url: "/pages/orderManage/clients/clients"
    })
  },

  productNameInput: function(event) {
        let productName = event.detail.value;
        this.setData({
            productName:productName
        })
  },

  numberInput: function(event) {
        let productNumber = event.detail.value;
        this.setData({
            productNumber: productNumber
        })
  },

  priceInput: function(event) {
        let productPrice = event.detail.value;
        this.setData({
            productPrice: productPrice
        })
  },

  submitTap: function(){
        let thirdSession = wx.getStorageSync('thirdSession');
        let customerid = this.data.client.id;
        let name = this.data.client.name;
        let email = this.data.client.email;
        let phone = this.data.client.phone;
        let province = this.data.client.province;
        let city = this.data.client.city;
        let country = this.data.client.country;
        let detail = this.data.client.detail;
        let productName = this.data.productName;
        let productNumber = this.data.productNumber;
        let productPrice = this.data.productPrice;
        let remark = this.data.remark;
        // 判断用户输入信息是否规范
        // @todo
        wx.request({
            url: 'https://xsounder.com/address/saveOrder/',
            method: 'POST',
            data: {
                thirdSession: thirdSession,
                name: name,
                email: email,
                phone: phone,
                customerid: customerid,
                province: province,
                city: city,
                country: country,
                detail: detail,
                productName: productName,
                productNumber: productNumber,
                productPrice: productPrice,
                remark: remark
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                wx.showToast({
                    title: '提交成功!',
                    icon: 'none',
                    duration: 2000,
                    mask: true,
                    success: function() {
                        setTimeout(function() { wx.navigateBack(); }, 2000)
                    }
                })
            }
        });
  }
})
