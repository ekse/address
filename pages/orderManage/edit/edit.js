//add.js
//获取应用实例
var app = getApp()

Page({
  data: {
    "client": {
        "id": 1,
        "name": "胥智",
        "phone": "18280086457",
        "province": "四川省",
        "city": "成都市",
        "country": "青羊区",
        "detail": "成飞大道199号",
        "sex": "男",
        "email": "5thgfka@gmail.com",
    },
    orderId: "",
    productName: "",
    productNumber: "",
    productPrice: "",
    "items": [
        {name: '1', value: '男'},
        {name: '0', value: '女'},
    ],
    "mode": "add",
    isClientSelect: true
  },
  
  onLoad: function (options) {
    console.log(options)
    let clientId = options.clientId;
    let orderId = options.orderId;
    let name = options.name;
    let province = options.province;
    let city = options.city;
    let country = options.country;
    let detail = options.detail;
    let phone = options.phone;
    let sex = options.sex;
    let email = options.email;
    let productPrice = options.productPrice;
    let productName = options.productName;
    let productNumber = options.productNumber;

    let items = this.data.items;
    items.map(function(it) {
        if(it.value == sex){
            it.checked = true;
        }
    });

    let client = {
        "id": clientId,
        "name": name,
        "province": province,
        "city": city,
        "country": country,
        "detail": detail,
        "sex": sex,
        "email": email,
        "phone": phone,
        "price": productPrice,
        "product": productName
    }
    this.setData({
        "client": client,
        "items": items,
        productName: productName,
        productPrice: productPrice,
        productNumber: productNumber,
        orderId: orderId
    })
  },


  submitTap: function(){
        let thirdSession = wx.getStorageSync('thirdSession');
        let customerid = this.data.client.id;
        let name = this.data.client.name;
        let email = this.data.client.email;
        let phone = this.data.client.phone;
        let province = this.data.client.province;
        let city = this.data.client.city;
        let country = this.data.client.country;
        let detail = this.data.client.detail;
        let productName = this.data.productName;
        let productNumber = this.data.productNumber;
        let productPrice = this.data.productPrice;
        let remark = this.data.remark;
        let orderId = this.data.orderId;
        // 判断用户输入信息是否规范
        // @todo
        wx.request({
            url: 'https://xsounder.com/address/updateOrder/',
            method: 'POST',
            data: {
                thirdSession: thirdSession,
                name: name,
                email: email,
                phone: phone,
                customerid: customerid,
                id: orderId,
                province: province,
                city: city,
                country: country,
                detail: detail,
                productName: productName,
                productNumber: productNumber,
                productPrice: productPrice,
                remark: remark
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                wx.showToast({
                    title: '提交成功!',
                    icon: 'none',
                    duration: 2000,
                    mask: true,
                    success: function() {
                        setTimeout(function() { wx.navigateBack(); }, 2000)
                    }
                })
            }
        });
  },

  productNameInput: function(event) {
        let productName = event.detail.value;
        this.setData({
            productName:productName
        })
  },

  numberInput: function(event) {
        let productNumber = event.detail.value;
        this.setData({
            productNumber: productNumber
        })
  },

  priceInput: function(event) {
        let productPrice = event.detail.value;
        this.setData({
            productPrice: productPrice
        })
  },


})
