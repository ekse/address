//orders.js
//获取应用实例
var app = getApp()

Page({
    data: {
        orderId: "",
        orderDate: "",
        clientId: "",
        clientName: "",
        orderPrice: "",
        province: "",
        city: "",
        country: "",
        detail: "",
        orderAddress: "",
        orderProduct: "",
        productCount: "",
        orderPhone: "",
        orderRemark: "",
    },

    onLoad: function(options) {
        let share = options.share;
        let orderId = options.orderId;
        let orderDate = options.orderDate;
        let clientId = options.clientId;
        let clientName = options.clientName;
        let productPrice = options.productPrice;
        let province = options.province;
        let city = options.city;
        let country = options.country;
        let detail = options.detail;
        let orderAddress = options.province + options.city + options.country + options.detail;
        let productName = options.productName;
        let productNumber = options.productNumber;
        let orderPhone = options.orderPhone;
        let orderRemark = options.orderRemark;

        let that = this;

        that.setData({
            orderId: orderId,
            orderDate: orderDate,
            clientId: clientId,
            clientName: clientName,
            productPrice: productPrice,
            province: province,
            city: city,
            country: country,
            detail: detail,
            orderAddress: orderAddress,
            productName: productName,
            productNumber: productNumber,
            orderPhone: orderPhone,
            orderRemark: orderRemark,
            share: share
        })
    },

    onEditTap: function() {
        wx.showActionSheet({
            itemList: ['备注'],
            success(res) {
                console.log(res.tapIndex)
            },
            fail(res) {
                console.log(res.errMsg)
            }
        })
    },

    onViewCustomerTap: function() {
        let that = this;
        let clientid = that.data.clientId;

        wx.navigateTo({
            url: "/pages/clientManage/client/client?clientid=" + clientid
        })
    },

    /**
    * 用户点击右上角分享
    */
    onShareAppMessage: function () {
        let that = this;
        let title = "订单详情";
        let orderId = that.data.orderId;
        let orderDate = that.data.orderDate;
        let clientId = that.data.clientId;
        let clientName = that.data.clientName;
        let productPrice = that.data.productPrice;
        let province = that.data.province;
        let city = that.data.city;
        let country = that.data.country;
        let detail = that.data.detail;
        let orderAddress = that.data.orderAddress;
        let productName = that.data.productName;
        let productNumber = that.data.productNumber;
        let orderPhone = that.data.orderPhone;
        let orderRemark = that.data.orderRemark;
        return {
            title: title,
            path: "/pages/orderManage/order/order?share=1&orderId=" + orderId + "&clientId=" + clientId + "&clientName=" + clientName + "&province=" + province 
                + "&city=" + city + "&country=" + country + "&detail=" + detail + "&orderPhone=" + orderPhone 
                + "&productPrice=" + productPrice + "&orderDate=" + orderDate + "&productName=" + productName + "&productNumber=" + productNumber + "&orderRemark=" + orderRemark,
            success: function(res) {
                // 转发成功
            },
            fail: function(res) {
                // 转发失败
            }
        }
    },
})
