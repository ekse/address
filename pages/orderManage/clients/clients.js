//apply.js
//获取应用实例
var app = getApp()

Page({
    data: {
        customers: [
            // {
            //     "id": 1,
            //     "name": "胥智",
            //     "phone": "18280086457",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "男",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 2,
            //     "style": ""
            // },
            // {
            //     "id": 2,
            //     "name": "王羽",
            //     "phone": "13608097916",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "女",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 1
            // },
            // {
            //     "id": 3,
            //     "name": "胥智",
            //     "phone": "18280086457",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "男",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 2
            // },
            // {
            //     "id": 4,
            //     "name": "王羽",
            //     "phone": "13608097916",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "女",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 1
            // },
            // {
            //     "id": 5,
            //     "name": "胥智",
            //     "phone": "18280086457",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "男",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 2
            // },
            // {
            //     "id": 6,
            //     "name": "王羽",
            //     "phone": "13608097916",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "女",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 1
            // }
        ],
        startX: "",
        delBtnWidth: 180,
        currentIdx: 0
    },

    selfData: {
        id: 0,
        name: null,
        phone: null,
        province: null,
        city: null,
        country: null,
        detail: null,
        sex: null,
        email: null,
    },

    onLoad: function() {},

    onShow: function() {
        let that = this;
        let thirdSession = wx.getStorageSync('thirdSession');
        wx.request({
            url: 'https://xsounder.com/address/getCustomers/',
            method: 'POST',
            data: {
                thirdSession: thirdSession,
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                // console.log(res)
                that.setData(res.data)
            }
        });
    },

    onSelectTap: function(e){
        let that = this;
        let index = e.currentTarget.dataset.index;
        let clientId = e.currentTarget.dataset.clientid;
        let name = e.currentTarget.dataset.name;
        let province = e.currentTarget.dataset.province;
        let city = e.currentTarget.dataset.city;
        let country = e.currentTarget.dataset.country;
        let detail = e.currentTarget.dataset.detail;
        let phone = e.currentTarget.dataset.phone;
        let sex = e.currentTarget.dataset.sex;
        let email = e.currentTarget.dataset.email;
        let remark = e.currentTarget.dataset.remark;

        that.selfData.id = clientId;
        that.selfData.name = name;
        that.selfData.province = province;
        that.selfData.city = city;
        that.selfData.country = country;
        that.selfData.detail = detail;
        that.selfData.phone = phone;
        that.selfData.sex = sex;
        that.selfData.email = email;
        that.selfData.remark = remark;


        console.log(that.selfData);

        that.setData({
            currentIdx: index
        })
    },

    submitTap: function() {
        let that = this;
        wx.setStorageSync("selectedClient", that.selfData);
        //跳转页面
        wx.navigateBack();
    },
    
})