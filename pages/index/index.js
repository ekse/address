//index.js
//获取应用实例
var app = getApp()
Page({
    data: {
        userInfo: {},
        userStatus: 0,
    },
    //事件处理函数
    bindViewTap: function() {
        wx.chooseAddress({
            success: function(res) {
                let userName = res.userName;
                let postalCode = res.postalCode;
                let provinceName = res.provinceName;
                let cityName = res.cityName;
                let countyName = res.countyName;
                let detailInfo = res.detailInfo;
                let telNumber = res.telNumber;
                // URL
                let url = '../detail/detail?userName=' + userName + '&postalCode=' + postalCode + '&provinceName=' + provinceName + '&cityName=' + cityName + '&countyName=' + countyName + '&detailInfo=' + detailInfo + '&telNumber=' + telNumber;
                wx.navigateTo({
                    url: url
                })
            }
        })
    },
    onLoad: function() {
        console.log("index onLoad");
        let that = this
        //调用应用实例的方法获取全局数据
        app.getUserInfo(function(userInfo) {
            //更新数据
            that.setData({
                userInfo: userInfo
            })
        });

        console.log(app.globalData.isIOS);
    },
    onShow: function(){
        console.log("index onShow");
        this.getMemberInfo();
    },
    onReady: function(){
        console.log("index onReady");
    },
    getMemberInfo: function(){
        let that = this;
        let thirdSession = wx.getStorageSync('thirdSession');
        wx.request({
            url: 'https://xsounder.com/address/getMemberInfo/',
            method: 'POST',
            data: {
                thirdSession: thirdSession,
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                let userStatus = res.data.userStatus;
                let memberStorageKey = app.globalData.memberStorage;
                wx.setStorageSync(memberStorageKey, res.data);
                that.setData({
                    userStatus: userStatus
                })
            }
        });
    },
    goFillApplication: function() {
      console.log(app.globalData.isIOS);
        wx.navigateTo({
            url: "/pages/apply/apply"
        })
    },
    goAdmin: function() {
        wx.navigateTo({
            url: "/pages/center/center"
        })
    }
})