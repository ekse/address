//search.js
//获取应用实例
var app = getApp()

Page({
    data: {
        showResults: true,
        clients: [
            // {
            //     "id": 1,
            //     "name": "胥智",
            //     "phone": "18280086457",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "男",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 2,
            //     "style": ""
            // },
            // {
            //     "id": 2,
            //     "name": "王羽",
            //     "phone": "13608097916",
            //     "province": "四川省",
            //     "city": "成都市",
            //     "country": "青羊区",
            //     "detail": "成飞大道199号",
            //     "sex": "女",
            //     "email": "5thgfka@gmail.com",
            //     "remark": "none",
            //     "count": 1
            // },
        ],
        startX: "",
        delBtnWidth: 180,
        inputStr: ""
    },

    onLoad: function(options) {},

    touchS: function(e) {
        if (e.touches.length == 1) {
            let sx = e.touches[0].clientX;
            // console.log("touchS", sx);
            this.setData({
                //设置触摸起始点水平方向位置
                startX: sx
            });
        }
    },
    touchM: function(e) {
        if (e.touches.length == 1) {
            //手指移动时水平方向位置
            let moveX = e.touches[0].clientX;
            //手指起始点位置与移动期间的差值
            let disX = this.data.startX - moveX;
            let delBtnWidth = this.data.delBtnWidth;
            let style = "";
            if (disX == 0 || disX < 0) { //如果移动距离小于等于0，说明向右滑动，文本层位置不变
                style = "left:0px";
            } else if (disX > 0) { //移动距离大于0，文本层left值等于手指移动距离
                style = "left:-" + disX + "px";
                if (disX >= delBtnWidth) {
                    //控制手指移动距离最大值为删除按钮的宽度
                    style = "left:-" + delBtnWidth + "px";
                }
            }
            //获取手指触摸的是哪一项
            let index = e.currentTarget.dataset.index;
            let clients = this.data.clients;

            clients.map(function(client, idx) {
                if (idx == index) {
                    client.style = style;
                } else {
                    client.style = "";
                }
            })

            // console.log("touchM", clients);
            //更新列表的状态
            this.setData({
                clients: clients
            });
        }
    },
    touchE: function(e) {
        if (e.changedTouches.length == 1) {
            //手指移动结束后水平位置
            let endX = e.changedTouches[0].clientX;
            //触摸开始与结束，手指移动的距离
            let disX = this.data.startX - endX;
            let delBtnWidth = this.data.delBtnWidth;
            //如果距离小于删除按钮的1/2，不显示删除按钮
            let style = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
            //获取手指触摸的是哪一项
            let index = e.currentTarget.dataset.index;
            let clients = this.data.clients;
            clients[index].style = style;
            // console.log("touchE", disX)
            // 判断为 0 的时候跳转浏览个人信息
            if (disX == 0) {
                console.log("跳转浏览客户信息")
            }
            //更新列表的状态
            this.setData({
                clients: clients
            });
        }
    },

    onEditTap: function(e) {
        let clientId = e.currentTarget.dataset.id;
        let name = e.currentTarget.dataset.name;
        let province = e.currentTarget.dataset.province;
        let city = e.currentTarget.dataset.city;
        let country = e.currentTarget.dataset.country;
        let detail = e.currentTarget.dataset.detail;
        let phone = e.currentTarget.dataset.phone;
        let sex = e.currentTarget.dataset.sex;
        let email = e.currentTarget.dataset.email;
        //跳转页面
        wx.navigateTo({
            url: "/pages/clientManage/edit/edit?clientid=" + clientId + "&name=" + name + "&province=" + province + "&city=" + city + "&country=" + country + "&detail=" + detail + "&phone=" + phone + "&sex=" + sex + "&email=" + email
        })
    },
    onDeleteTap: function(e) {
        let clientId = e.currentTarget.dataset.id;
        wx.showModal({
            title: "提示",
            content: "确定删除用户信息吗？",
            success: function(res) {
                if (res.confirm) {
                    console.log('用户点击确定', clientId);
                } else if (res.cancel) {
                    console.log('用户点击取消', clientId);
                }
            }
        })
    },

    onInputChange: function(e){
        let that = this;
        let inputStr = e.detail.value;

        that.setData({
            inputStr: inputStr
        })
    },

    confirmSearch: function(e){
        let that = this;
        let inputStr = that.data.inputStr;
        if(inputStr.length == 0){
            wx.showToast({
                title: '请先输入搜索内容',
                icon: 'none',
                duration: 2000
            });
            return;
        }
        let thirdSession = wx.getStorageSync('thirdSession');
        wx.request({
            url: 'https://xsounder.com/address/searchCustomers/',
            method: 'POST',
            data: {
                thirdSession: thirdSession,
                queryString: inputStr
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                let customers = res.data.customers;
                if(customers.length == 0){
                    wx.showToast({
                        title: '未找到结果',
                        icon: 'none',
                        duration: 2000
                    });
                }
                else {
                    that.setData({
                        clients: customers
                    });
                }
            }
        });
    }
})