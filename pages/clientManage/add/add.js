//add.js
//获取应用实例
var app = getApp()

Page({
    data: {
        id: 1,
        name: "",
        phone: "",
        province: "",
        city: "",
        country: "",
        detail: "",
        sex: "",
        email: "",
        remark: "",
        items: [
            { name: '1', value: '男' },
            { name: '0', value: '女' },
        ],
    },

    onLoad: function(options) {},
    // 输入框事件函数
    nameInput: function(event) {
        let ipts = event.detail.value;
        this.setData({
            name: ipts
        })
    },
    phoneInput: function(event) {
        let ipts = event.detail.value;
        this.setData({
            phone: ipts
        })
    },
    emailInput: function(event) {
        let ipts = event.detail.value;
        this.setData({
            email: ipts
        })
    },
    remarkInput: function(event) {
        let ipts = event.detail.value;
        this.setData({
            remark: ipts
        })
    },
    radioChange: function(event){
        let ipts = event.detail.value;
        this.setData({
            sex: ipts
        });
    },
    addressTap: function() {
        let that = this;
        wx.chooseAddress({
            success(res) {
                that.setData({
                    province: res.provinceName,
                    city: res.cityName,
                    country: res.countyName,
                    detail: res.detailInfo,
                    phone: res.telNumber,
                    name: res.userName
                })
                // console.log(res.userName)
                // console.log(res.postalCode)
                // console.log(res.provinceName)
                // console.log(res.cityName)
                // console.log(res.countyName)
                // console.log(res.detailInfo)
                // console.log(res.nationalCode)
                // console.log(res.telNumber)
            }
        })
    },
    // 提交数据
    submitTap: function(){
        // 保存用户申请
        let thirdSession = wx.getStorageSync('thirdSession');
        let name = this.data.name;
        let email = this.data.email;
        let phone = this.data.phone;
        let remark = this.data.remark;
        let province = this.data.province;
        let city = this.data.city;
        let country = this.data.country;
        let detail = this.data.detail;
        let sex = this.data.sex;
        // 判断用户输入信息是否规范
        if(name.trim() == "" || email.trim() == "" ||
            phone.trim() == "" || province.trim() == "" ||
            city.trim() == "" || country.trim() == "" ||
            detail.trim() == "" || sex.trim() == "") {
            wx.showToast({
                title:"请完成表格内容",
                icon: "none",
                duration: 2000
            })
            return;
        }
        wx.request({
            url: 'https://xsounder.com/address/addCustomer/',
            method: 'POST',
            data: {
                thirdSession: thirdSession,
                name: name,
                email: email,
                phone: phone,
                province: province,
                city: city,
                country: country,
                detail: detail,
                sex: sex,
                remark: remark
            },
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function(res) {
                wx.showToast({
                    title: '提交成功!',
                    icon: 'none',
                    duration: 2000,
                    mask: true,
                    success: function() {
                        setTimeout(function() { wx.navigateBack(); }, 2000)
                    }
                })
            }
        });
    }
})