//detail.js
Page({
  data: {
  },
  onLoad: function (options) {
    var userName = options.userName;
    var postalCode = options.postalCode;
    var provinceName = options.provinceName;
    var cityName = options.cityName;
    var countyName = options.countyName;
    var detailInfo = options.detailInfo;
    var telNumber = options.telNumber;

    this.setData({
      userName: userName,
      postalCode: postalCode,
      provinceName: provinceName,
      cityName: cityName,
      countyName: countyName,
      detailInfo: detailInfo,
      telNumber: telNumber,
      comment: ""
    });

    this.drawShareImage();
  },
  commentChange: function(e){
    var comment = e.detail.value;
    this.setData({
      comment: comment
    })
  },

  redraw: function(){
    this.drawShareImage();
  },

  onShareAppMessage: function () {
    let that = this;
    let userName = this.data.userName;
    let postalCode = this.data.postalCode;
    let provinceName = this.data.provinceName;
    let cityName = this.data.cityName;
    let countyName = this.data.countyName;
    let detailInfo = this.data.detailInfo;
    let telNumber = this.data.telNumber;
    let comment = this.data.comment;
    

    return {
      title: '我的地址',
      path: '/pages/share/share?userName=' + userName + '&postalCode=' + postalCode + '&provinceName=' + provinceName + '&cityName=' + cityName + '&countyName=' + countyName + '&detailInfo=' + detailInfo + '&telNumber=' + telNumber + '&comment=' + comment,
      imageUrl: that.data.sharePath,
      success: function(res) {
        // 转发成功
      },
      fail: function(res) {
        // 转发失败
      }
    }
  },

  drawShareImage: function(){
    let that = this;
    let ctx = wx.createCanvasContext('shareCanvas');
    ctx.setFillStyle("black");
    // 姓名
    ctx.setFontSize(48);
    ctx.fillText(this.data.userName, 62, 62);
    // 电话
    ctx.setFontSize(25);
    ctx.fillText(this.data.telNumber, 62, 134);
    // address
    ctx.setFontSize(30);
    let address = this.data.provinceName + this.data.cityName + this.data.countyName;
    ctx.fillText(address, 62, 190);
    // 详细地址
    ctx.setFontSize(30);
    ctx.fillText(this.data.detailInfo, 62, 233);
    // 邮编
    ctx.setFillStyle("grey");
    ctx.setFontSize(25);
    ctx.fillText(this.data.postalCode, 62, 280);
    // 备注
    if(this.data.comment != ""){
      ctx.setFontSize(30);
      ctx.setFillStyle("red")
      ctx.fillText("留言：" + this.data.comment, 62, 329);
    }
    

    ctx.draw(false, function(){
      wx.canvasToTempFilePath({
        canvasId: "shareCanvas",
        destWidth: 500,
        destHeight: 400,
        quality: 1,
        success: function(res){
          let tempFilePath = res.tempFilePath;

          that.setData({
            sharePath: tempFilePath
          })
        }
      })
    })
  }
})
