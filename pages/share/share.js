//detail.js
Page({
  data: {
  },
  onLoad: function (options) {
    var userName = options.userName;
    var postalCode = options.postalCode;
    var provinceName = options.provinceName;
    var cityName = options.cityName;
    var countyName = options.countyName;
    var detailInfo = options.detailInfo;
    var telNumber = options.telNumber;
    var comment = options.comment;

    this.setData({
      userName: userName,
      postalCode: postalCode,
      provinceName: provinceName,
      cityName: cityName,
      countyName: countyName,
      detailInfo: detailInfo,
      telNumber: telNumber,
      comment: comment
    })
  },
  onShareAppMessage: function () {
    var userName = this.data.userName;
    var postalCode = this.data.postalCode;
    var provinceName = this.data.provinceName;
    var cityName = this.data.cityName;
    var countyName = this.data.countyName;
    var detailInfo = this.data.detailInfo;
    var telNumber = this.data.telNumber;
    var comment = this.data.comment;
    return {
      title: '我的地址',
      path: '/pages/share/share?userName=' + userName + '&postalCode=' + postalCode + '&provinceName=' + provinceName + '&cityName=' + cityName + '&countyName=' + countyName + '&detailInfo=' + detailInfo + '&telNumber=' + telNumber + '&comment=' + comment,
      success: function(res) {
        // 转发成功
      },
      fail: function(res) {
        // 转发失败
      }
    }
  },
  copyAddress: function(){

    var userName = this.data.userName;
    var postalCode = this.data.postalCode;
    var provinceName = this.data.provinceName;
    var cityName = this.data.cityName;
    var countyName = this.data.countyName;
    var detailInfo = this.data.detailInfo;
    var telNumber = this.data.telNumber;
    var comment = this.data.comment;
    var data = userName + ' ' + postalCode + ' ' + provinceName + cityName + countyName + detailInfo + ' ' + telNumber;
    wx.setClipboardData({
      data: data,
      success: function(res) {
      }
    });
  }
})
